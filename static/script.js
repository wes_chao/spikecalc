var lead_skills;
var awakenings;
/*	1: Enhanced HP, 2: Enhanced Attack, 3: Enhanced Heal, 4: Reduce Fire Damage, 5: Reduce Water Damage, 6: Reduce Wood Damage, 7: Reduce Light Damage,
	8: Reduce Dark Damage, 9: Auto-Recover, 10: Resistance-Bind, 11: Resistance-Dark, 12: Resistance-Jammers, 13: Resistance-Poison, 14: Enhanced Fire Orbs,
	15: Enhanced Water Orbs, 16: Enhanced Wood Orbs, 17: Enhanced Light Orbs, 18: Enhanced Dark Orbs, 19: Extend Time, 20: Recover Bind, 21: Skill Boost
	22: Enhanced Fire Att., 23: Enhanced Water Att., 24: Enhanced Wood Att., 25: Enhanced Light Att., 26: Enhanced Dark Att., 27: Two-Prong Attack, 28: Resistance-Skill Lock*/
var monst_list;
var running = false;
var ajxRan = [0, 0, []]; // Times to run, times run, list to use in callback.
// TODO: Refactor code to have team list exist in seperate variable.

var awoken_disabled = 0;
var sub_binds = [0, 0, 0, 0, 0, 0];

const MONSTER_MOD_NONE = 0;
const MONSTER_MOD_MAX_LEVEL = 1;
const MONSTER_MOD_HYPERMAX = 2;

var current_monster_modification = MONSTER_MOD_NONE;

// See if we are trying to permalink to a particular team
var QueryString = function () {
    var query_string = {};
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i=0;i<vars.length;i++) {
        var pair = vars[i].split("=");
        // If first entry with this name
        if (typeof query_string[pair[0]] === "undefined") {
            query_string[pair[0]] = decodeURIComponent(pair[1]);
            // If second entry with this name
        } else if (typeof query_string[pair[0]] === "string") {
            var arr = [ query_string[pair[0]],decodeURIComponent(pair[1]) ];
            query_string[pair[0]] = arr;
            // If third or later entry with this name
        } else {
            query_string[pair[0]].push(decodeURIComponent(pair[1]));
        }
    }
    return query_string;
}();

function loadTeam(){
    // initialize variables
    var team = [];
    team.enhance = [0, 0, 0, 0, 0, 0]; // Row enhance, per element.
    team.ind_enh = [[0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0]];
    team.orbs = [0, 0, 0, 0, 0, 0];
    team.ind_orb = [[0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0], [0, 0, 0, 0, 0, 0]];
    team.sb = [0, 0, 0, 0, 0, 0];
    team.sbr = [0, 0, 0, 0, 0, 0];
    ajxRan = [6, 0, team];

    $('#about-block').hide();
    $('#manual-entry').hide();

    // if we're loading a new team, reset awokens
    awoken_disabled = 0;
    $('a[id=awoken-toggle]').text("disable awokens");

    // if this was a deeplink, we may not have gotten monsters yet. Wait until we do.
    if (! monst_list)
        setTimeout(loadTeam, 100);
    else {

        var id = $("#team-id-form input[name=team-id]").val();
        if (running){
            return false;
        }

        if (!id) {
            $('#team-block').removeClass('hidden');
            return;
        }

        if (isNaN(id)){
            ind = id.search("/teams/#");
            if (ind == -1){
                return;
            }
            id = id.substr(ind+8);
        }

        if (!$('#ad-block').hasClass('hidden')){ $('#ad-block').addClass('hidden'); }
        running = true;
        $.ajax( "https://www.padherder.com/user-api/team/" + id + "/", {dataType: 'json', success: function(a, b, c){
                    buildTeam(a);
                }} );
        return false;
    }
}

function setCombos (value) {
    var existing_combo_rows = $('[id^=combo-row]').length;

    // erase all the existing rows
    for (var ii = 0; ii < existing_combo_rows; ii++)
        $('#minus-combo-btn').click();

    // add the new rows
    for (var ii = 0; ii < value; ii++)
        $('#plus-combo-btn').click();
}

function updateComboColor(comboRow, val) {
    var button = $('#combo-drop-' + comboRow);
    var img = '/images/' + ({0:'fr',1:'wt',2:'wd',3:'lt',4:'dk',5:'ht'})[val] + '.png';

    button.val(val);
    button.find('img').attr('src', img);
    img = img.replace('.', '+.');
    $('.combo-row:nth-of-type(' + comboRow + ')').find('span img').attr('src', img);
}

function changeLeaderSkill(value) {
    skillMultiplierHelper('#leader-field', 'input[name=leader-mult]', value);
}
function changeActiveSkill(value) {
    skillMultiplierHelper('#active-field', 'input[name=active-mult]', value);
}

function resetSkillMultipliers() {
    changeLeaderSkill (1);
    changeActiveSkill (1);
    $('select[name=leader-cond1').val("");
    $('select[name=leader-cond2').val("");
    $('select[name=act-cond1').val("");
    $('select[name=act-cond2').val("");
}

function skillMultiplierHelper(textFieldName, radioButtonName, value) {
    $(textFieldName).val(value);
    var possible_multipliers = $(radioButtonName);
    possible_multipliers.each (function (index, element) {
            if ($(element).val() == value)
                $(element).prop('checked', true);
        });
}

// What a hideous onload.
$(document).ready( function(){
	// Load the json files.
	/*$.ajax('https://www.padherder.com/api/leader_skills/', {dataType: 'json', success: function(a, b, c){lead_skills = a;}});
	$.ajax('https://www.padherder.com/api/awakenings/', {dataType: 'json', success: function(a, b, c){awakenings = a;}});
	$.ajax('https://www.padherder.com/api/monsters/', {dataType: 'json', success: function(a, b, c){
		monst_list = a;
		$('div.loader').addClass('hidden');
		$('#team-id-form').removeClass('hidden');
	}});*/
	$.ajax('leader_skills.json', {dataType: 'json', success: function(a, b, c){lead_skills = a;}});
	$.ajax('awakenings.json', {dataType: 'json', success: function(a, b, c){awakenings = a;}});
	$.ajax('monsters.json', {dataType: 'json', success: function(a, b, c){
		monst_list = a;
		$('div.loader').addClass('hidden');
		$('#team-id-form').removeClass('hidden');
	}});

	if (!$('#team-block').hasClass('hidden')){ $('#team-block').addClass('hidden'); }
	if (!$('#results-block').hasClass('hidden')){ $('#results-block').addClass('hidden'); }
	if (!$('#team-id-form').hasClass('hidden')){ $('#team-id-form').addClass('hidden'); }

	// Populate select fields.
	for (var i = 0; i <= 4; i++){
		$('select optgroup[label=Element]').append("<option value='elem-" + i + "'>" + elements[i] + "</option>");
	}
	for (var i = 1; i <= 7; i++){
		$('select optgroup[label=Type]').append("<option value='type-" + i + "'>" + types[i] + "</option>");
	}

	$('#team-id-form').on('submit', loadTeam);

        // Handler for manual entry
        $('a[id=manual-entry]').on('click', function(e){
                loadTeam();
        });

	// Handler for active skill radio buttons.
	$('input[name=active-mult]').on('change', function(e){
		$('#active-field').val(e.target.value);
	});

	// Handler for leader conditional radio buttons.
	$('input[name=leader-mult]').on('change', function(e){
		$('#leader-field').val(e.target.value);
	});

        // Handler for hypermax buttons
        $('a[id=normal_toggle]').on('click', function(e){
                current_monster_modification = 0;
                recalcMonsters();
        });
        $('a[id=maxlevel_toggle]').on('click', function(e){
                current_monster_modification = 1;
                recalcMonsters();
        });
        $('a[id=hypermax_toggle]').on('click', function(e){
                current_monster_modification = 2;
                recalcMonsters();
        });

        // Handler for Hera pre-emptive display
        $('a[id=hera-toggle]').on('click', function(e){
                if ($('div[id=hera]').hasClass('hidden'))
                    $('div[id=hera]').removeClass('hidden');
                else
                    $('div[id=hera]').addClass('hidden');
        });


	// Handler for awoken toggle
        $('a[id=awoken-toggle]').on('click', function(e){
                awoken_disabled = (awoken_disabled) ? 0 : 1;

                // re-render the awakenings, either with a gray filter or without
                for (var ii = 0; ii < 6; ii++)
                    displayAwakenings(ii);

                // change the toggle text
                awoken_toggle_text = (awoken_disabled) ? "enable awokens" : "disable awokens";
                $('a[id=awoken-toggle]').text(awoken_toggle_text);

                // recalculate atk stat for monsters with +atk awakenings
                recalcMonsters();
        });

        // Handler for latent toggles
        var latent_selectors = $('[name^=latent-pos]');
        latent_selectors.each ( function (index, element) {
                $(element).on('change', function(e) {

                        // figure out which monster this is, using the last character as the index
                        var monster_index = Number( e.target.name.charAt(e.target.name.length-1));
                        var monster = ajxRan[2][monster_index];

                        // add the awakening to the object model, pushing out the last one if there are 5
                        if (monster.latents.length == 5)
                            monster.latents.splice (0, 1);

                        monster.latents.push(e.target.value);

                        // recalculate hp/atk/rcv if necessary
                        if (e.target.value == "hp" ||
                            e.target.value == "atk" ||
                            e.target.value == "rcv")
                        monster[e.target.value] = monster.statCalc(e.target.value);

                        // reset the box so we can add another
                        $(element).val("");

                        // update display with recalculated numbers and icons
                        buildTeamTable(ajxRan[2]);
                });
            });

        // Set resist toggle default
        $('select[id=enemy-resist1').val("elem-3");
        $('select[id=enemy-resist2').val("elem-4");

	// Handler for resist toggle
        $('a[id=enemy-resist-toggle]').on('click', function(e){
                var is_hidden = $('label[name=resist-label]').hasClass('hidden');

                if (is_hidden) {
                    $('label[name=resist-label').removeClass('hidden');
                    $('div[name=resist-input').removeClass('hidden');
                    $('a[id=enemy-resist-toggle]').text("Hide");

                    $('input[id=resist-percentage').val(50);
                }
                else {
                    $('label[name=resist-label').addClass('hidden');
                    $('div[name=resist-input').addClass('hidden');
                    $('a[id=enemy-resist-toggle]').text("Resist");

                    $('input[id=resist-percentage').val(0);
                }
        });

        // Handler for absorb toggle
        $('a[id=enemy-absorb-toggle]').on('click', function(e){
                var is_hidden = $('label[name=absorb-label]').hasClass('hidden');

                if (is_hidden) {
                    $('label[name=absorb-label').removeClass('hidden');
                    $('div[name=absorb-input').removeClass('hidden');
                    $('a[id=enemy-absorb-toggle]').text("Hide");
                }
                else {
                    $('label[name=absorb-label').addClass('hidden');
                    $('div[name=absorb-input').addClass('hidden');
                    $('a[id=enemy-absorb-toggle]').text("Absorb");

                    $('select[id=enemy-absorb').val("");
                    $('input[id=absorb-threshold').val("");
                }
        });

        // Handler for typical combo setups
        $('select[id=combo-setup]').on('change', function(e){
                resetSkillMultipliers();

                if ($('select[id=combo-setup]').val() == "beelze"){
                    setCombos(6);

                    // set the colors on the first three to dark and the next three to heart
                    updateComboColor(1, 4);
                    updateComboColor(2, 4);
                    updateComboColor(3, 4);
                    updateComboColor(4, 5);
                    updateComboColor(5, 5);
                    updateComboColor(6, 5);

                    // set combos to an optimal 21-9 board
                    var combo_rows = $('div[id^="combo-row"]');
                    combo_rows.each( function (index, element) {
                            if (index < 2) {
                                // find the orb input box and set it
                                var orbs = $(element).find('input[name=orbs]')
                                orbs.val(9);
                                orbs.change();

                                // enhance!
                                $(element).find('input[name=porb]').val(9);

                                // find the row checkbox and check it
                                var row_number = index + 1;
                                $(element).find('div:last input[type=checkbox]').prop("checked", true);
                            }
                            else {
                                $(element).find('input[name=orbs]').val(3);
                                if (index == 2)
                                    $(element).find('input[name=porb]').val(3);
                            }
                    });

                    // set active skill to 1.5x devil
                    $('select[name=act-cond1').val("type-7");
                    changeActiveSkill(1.5);

                    // set conditional leader skill to 1.2x all
                    $('select[name=leader-cond1').val("all");
                    changeLeaderSkill(1.2);
                }

                if ($('select[id=combo-setup]').val() == "freyja"){
                    setCombos(6);

                    // set the colors on the first three to green and the others to heart
                    updateComboColor(1, 2);
                    updateComboColor(2, 2);
                    updateComboColor(3, 2);
                    updateComboColor(4, 5);
                    updateComboColor(5, 5);
                    updateComboColor(6, 5);

                    // set combos to an optimal 21-9 board
                    var combo_rows = $('div[id^="combo-row"]');
                    combo_rows.each( function (index, element) {
                            if (index < 2) {
                                // find the orb input box and set it
                                var orbs = $(element).find('input[name=orbs]')
                                orbs.val(9);
                                orbs.change();

                                // find the row checkbox and check it
                                var row_number = index + 1;
                                $(element).find('div:last input[type=checkbox]').prop("checked", true);
                            }
                            else
                                $(element).find('input[name=orbs]').val(3);
                    });

                    // set active skill to 2x wood/dark
                    $('select[name=act-cond1').val("elem-2");
                    $('select[name=act-cond2').val("elem-4");
                    changeActiveSkill(2);

                    // set conditional leader skill to 1.69x all
                    $('select[name=leader-cond1').val("all");
                    changeLeaderSkill(1.69);
                }

                if ($('select[id=combo-setup]').val() == "verdandi"){
                    setCombos(6);

                    // set the colors to two green, two red, two heart
                    updateComboColor(1, 2);
                    updateComboColor(2, 2);
                    updateComboColor(3, 0);
                    updateComboColor(4, 0);
                    updateComboColor(5, 5);
                    updateComboColor(6, 5);

                    // set combos to 2 wood TPA, each with 1 enhanced orb, match-3 otherwise
                    var combo_rows = $('div[id^="combo-row"]');
                    combo_rows.each( function (index, element) {
                            if (index < 2) {
                                // find the orb input box and set it
                                var orbs = $(element).find('input[name=orbs]')
                                orbs.val(4);
                                orbs.change();

                                $(element).find('input[name=porb]').val(1);
                            }
                            else
                                $(element).find('input[name=orbs]').val(3);
                    });

                    // set active skill to 2x wood/dark
                    $('select[name=act-cond1').val("elem-2");
                    $('select[name=act-cond2').val("elem-4");
                    changeActiveSkill(2);

                    // set conditional leader skill to all
                    $('select[name=leader-cond1').val("all");
                }

                if ($('select[id=combo-setup]').val() == "bastet"){
                    setCombos(7);

                    // set the colors on the first six to rainbow
                    for (var ii = 0; ii < 6; ii++) {
                        updateComboColor(ii, ii);
                    }

                    // set combos to 3 with a green TPA
                    var combo_rows = $('input[name=orbs]');
                    combo_rows.each( function (index, element) {
                            if (index == 1)
                                $(element).val(4);
                            else
                                $(element).val(3);
                    });

                    // set the conditional leader skill to 2.25x wood
                    $('select[name=leader-cond1').val("elem-2");
                    changeLeaderSkill(2.25);
                }

                if ($('select[id=combo-setup]').val() == "shiva"){
                    setCombos(6);

                    // set the colors on the first six to rainbow
                    for (var ii = 0; ii < 6; ii++) {
                        updateComboColor(ii, ii);
                    }

                    // set combos to 3 with a green TPA
                    var combo_rows = $('input[name=orbs]');
                    combo_rows.each( function (index, element) {
                            if (index == 5)
                                $(element).val(4);
                            else
                                $(element).val(3);
                    });

                    // set the conditional leader skill to 9x all
                    $('select[name=leader-cond1').val("all");
                    changeLeaderSkill(9);
                }

                if ($('select[id=combo-setup]').val() == "ra"){
                    setCombos(5);

                    // set the colors on the first five to rainbow
                    for (var ii = 0; ii < 5; ii++) {
                        updateComboColor(ii, ii);
                    }

                    // set combos to 3
                    var combo_rows = $('input[name=orbs]');
                    combo_rows.each( function (index, element) {
                            $(element).val(3);
                    });

                    // set the conditional leader skill to 4x god/devil
                    $('select[name=leader-cond1').val("type-5");
                    $('select[name=leader-cond2').val("type-7");
                    changeLeaderSkill(4);
                }

                if ($('select[id=combo-setup]').val() == "yomi"){
                    setCombos(7);

                    // set the colors on the first six to rainbow
                    for (var ii = 0; ii < 6; ii++) {
                        updateComboColor(ii, ii);
                    }

                    // set combos to 3 with a dark match-5
                    var combo_rows = $('input[name=orbs]');
                    combo_rows.each( function (index, element) {
                            if (index == 3)
                                $(element).val(5);
                            else
                                $(element).val(3);
                    });

                    // add a + orb to the dark match-5
                    var combo_plus = $('input[name=porb]');
                    combo_plus.each( function (index, element) {
                            if (index == 3)
                                $(element).val(1);
                    });

                    // set the active skill to 9x dark
                    $('select[name=act-cond1').val("elem-4");
                    changeActiveSkill(9);

                    // set leader skill to "all"
                    $('select[name=leader-cond1').val("all");
                }
        });

	// Damage form handlers.
	$('#dmg-form').on('submit', function(){ return false; });
	$('#dmg-submit').on('click', function(){
                calcDamage();
		return false;
	});

	// Add combo button handler.
	$('#plus-combo-btn').on('click', function(){
		var node = $('.combo-row:last');
		if (node.length == 0){
			var count = 1;
			node = $('#combo-label');
		}
		else {
			// Multiply for int conversion.
			var count = (node.attr('id').substr(10) * 1) + 1;
		}

		// This is obnoxious.
		var elem = '<div id="combo-row-' + count + '" class="row combo-row">' +
						'<div class="col-md-1 col-md-offset-2">' +
							'<b><p class="form-control-static">Combo ' + count + '</p></b>' +
						'</div>' +
						'<div class="col-md-3">' +
							'<div class="input-group">' +
								'<input class="form-control" name="orbs" type="number" min="3" max="30" step="1" Placeholder="# of Orbs"></input>' +
								'<div class="input-group-btn">' +
									'<button class="btn btn-default dropdown-toggle img-fit" type="button" id="combo-drop-' + count + '" data-toggle="dropdown" value="0">' +
										'<img src="/images/fr.png"/>' +
										'<span class="caret"></span>' +
									'</button>' +
									'<ul class="dropdown-menu" role="menu" aria-labelledby="combo-drop-' + count + '">' +
										'<li value=0 role="presentation"><a class="text-center" role="menuitem" tabindex="-1"><img src="/images/fr.png"/></a></li>' +
										'<li value=1 role="presentation"><a class="text-center" role="menuitem" tabindex="-1"><img src="/images/wt.png"/></a></li>' +
										'<li value=2 role="presentation"><a class="text-center" role="menuitem" tabindex="-1"><img src="/images/wd.png"/></a></li>' +
										'<li value=3 role="presentation"><a class="text-center" role="menuitem" tabindex="-1"><img src="/images/lt.png"/></a></li>' +
										'<li value=4 role="presentation"><a class="text-center" role="menuitem" tabindex="-1"><img src="/images/dk.png"/></a></li>' +
										'<li value=5 role="presentation"><a class="text-center" role="menuitem" tabindex="-1"><img src="/images/ht.png"/></a></li>' +
									'</ul>' +
								'</div>' +
							'</div>' +
						'</div>' +
						'<div class="col-md-1">' +
							'<p class="form-control-static orbs-with">Orbs with</p>' +
						'</div>' +
						'<div class="col-md-3">' +
							'<div class="input-group">' +
								'<input class="form-control" name="porb" type="number" min="0" max="30" step="1" Placeholder="# of +Orbs"></input>' +
								'<span class="input-group-addon img-fit">' +
									'<img src="/images/fr+.png"/>' +
								'</span>' +
							'</div>' +
						'</div>' +
						'<div class="col-md-1 hidden">' +
							'<p class="form-control-static check-row">Row? <input class="pull-right" type="checkbox"></input></p>' +
						'</div>' +
					'</div>';

		node.after(elem);

		// Add handlers for the input fields.
		node = $('.combo-row:last');
		node.find('input[name=orbs]').on('change', {index: count}, function(e){
			var parent = $('.combo-row:nth-of-type(' + e.data.index + ')');
			if (!parent.length){
				// Well, this is awkward.
				return;
			}

			var val = e.target.value;
			if (val < 3){
				e.target.value = 0;
				return;
			}
			else if (val > 30){
				e.target.value = 30;
				val = 30;
			}

			if (parent.find('input[name=porb]').val() > val){
				parent.find('input[name=porb]').val(val);
			}
			parent.find('input[name=porb]').attr('max', val);

			if (val >= 5){
				parent.find('div:last').removeClass('hidden');
			}
			else {
				parent.find('div:last').addClass('hidden');
				// Now you see me, now you don't... Joke's on you, I'm still around
				// if the value becomes smaller than 6 uncheck the box
				parent.find('div:last input[type=checkbox]').prop("checked", false);
			}
		});
		node.find('ul').on('click', {index: count}, function(e){
                        updateComboColor(e.data.index, $(e.target).parents('li').val());
                });
        }); // closes the handler function for the +combo button

	// Add combo button handler.
	$('#minus-combo-btn').on('click', function(){
		var node = $('.combo-row:last');
                node.remove();
            });

        // Handler for sub binds
        var sub_bind_toggles = $('[name^=sub-bind-toggle]');
        sub_bind_toggles.each ( function (index, element) {
                $(element).on('click', function(e) {

                        // figure out which monster this is, using the last character as the index
                        var monster_index = Number( e.target.name.charAt(e.target.name.length-1) ) - 1;

                        // toggle the bind
                        if (sub_binds[monster_index]) {
                            sub_binds[monster_index] = 0;
                            $(element).text("Bind this sub");
                        }
                        else {
                            sub_binds[monster_index] = 1;
                            $(element).text("Unbind");
                        }

                        // recalculate ATK total
                        calcDamage();
                });
            });


        // create one combo button to start
	$('#plus-combo-btn').click();

	// Bind functions to update +orb fields.
	$('#about-link').on('click', function(e){
		if (!$('#team-block').hasClass('hidden')){
			$('#team-block').fadeToggle();
			$('#results-block').fadeToggle();
			$('#about-block').delay(410).fadeToggle(205);
		}
		else {
			$('#about-block').fadeToggle();
		}
	});

	// Set editable fields in monster display table + handlers.
	$('span.editable').bind('click', function() {
                $(this).attr('contentEditable', true);
                $(this).focus();
                document.execCommand('selectAll', false, null);
            }).blur(
        function() {
            $(this).attr('contentEditable', false);
			if ( $(this).hasClass('level') ) { // Editing level fields
				var level = Number( new RegExp(/[1-9][0-9]|[1-9]/).exec( $(this).text() ) ) || 1;
				var team_no = Number( $(this).parent().attr('class').slice(-1) );
				level = Math.min(level, ajxRan[2][team_no-1].max_level);
				$(this).text(level);
				ajxRan[2][team_no-1].level = level;
				ajxRan[2][team_no-1].hp = ajxRan[2][team_no-1].statCalc('hp');
				ajxRan[2][team_no-1].atk = ajxRan[2][team_no-1].statCalc('atk');
				ajxRan[2][team_no-1].rcv = ajxRan[2][team_no-1].statCalc('rcv');
				buildTeamTable(ajxRan[2]);
			}

			else if ( $(this).hasClass('plus') ) { // Editing +egg fields
				var eggs = Number( new RegExp(/[1-9][0-9]|[1-9]/).exec( $(this).text() ) ) || 0;
				$(this).text( "(+" + eggs + ")" );
				var team_no = Number( $(this).parent().attr('class').slice(-1) );
				var stat = $(this).parent().parent().attr('id').split('-')[0];
				ajxRan[2][team_no-1].pluses[stat] = eggs;
				ajxRan[2][team_no-1][stat] = ajxRan[2][team_no-1].statCalc(stat);
				buildTeamTable(ajxRan[2]);
			}

			else if ( $(this).hasClass('awoken') ) { // Editing awoken skill fields
				var awoken = Number( new RegExp(/[1-9][0-9]|[1-9]/).exec( $(this).text() ) ) || 0;
				var team_no = Number( $(this).parent().attr('class').slice(-1) );
				awoken = Math.min(awoken, ajxRan[2][team_no-1].all_awake.length);
				$(this).text( awoken );
				ajxRan[2][team_no-1].awakenings = ajxRan[2][team_no-1].all_awake.slice(0, awoken);
				
				// Recalc stats in-case of stat awakenings.
				ajxRan[2][team_no-1].hp = ajxRan[2][team_no-1].statCalc('hp');
				ajxRan[2][team_no-1].atk = ajxRan[2][team_no-1].statCalc('atk');
				ajxRan[2][team_no-1].rcv = ajxRan[2][team_no-1].statCalc('rcv');

				// Re-calc row enhance, and orb-enhance numbers.
				ajxRan[2].ind_enh[team_no-1][0] = ajxRan[2][team_no-1].countAwaken(22);
				ajxRan[2].ind_enh[team_no-1][1] = ajxRan[2][team_no-1].countAwaken(23);
				ajxRan[2].ind_enh[team_no-1][2] = ajxRan[2][team_no-1].countAwaken(24);
				ajxRan[2].ind_enh[team_no-1][3] = ajxRan[2][team_no-1].countAwaken(25);
				ajxRan[2].ind_enh[team_no-1][4] = ajxRan[2][team_no-1].countAwaken(26);

				ajxRan[2].ind_orb[team_no-1][0] = ajxRan[2][team_no-1].countAwaken(14);
				ajxRan[2].ind_orb[team_no-1][1] = ajxRan[2][team_no-1].countAwaken(15);
				ajxRan[2].ind_orb[team_no-1][2] = ajxRan[2][team_no-1].countAwaken(16);
				ajxRan[2].ind_orb[team_no-1][3] = ajxRan[2][team_no-1].countAwaken(17);
				ajxRan[2].ind_orb[team_no-1][4] = ajxRan[2][team_no-1].countAwaken(18);
				ajxRan[2].ind_orb[team_no-1][5] = ajxRan[2][team_no-1].countAwaken(29);

				// Skill Boosts and Skill Bind Resist
				ajxRan[2].sb[team_no-1] = ajxRan[2][team_no-1].countAwaken(21);
				ajxRan[2].sbr[team_no-1] = ajxRan[2][team_no-1].countAwaken(28);
				buildTeamTable(ajxRan[2]);
			}

			else if ( $(this).hasClass('id-box') ) { // Editing monster ID
				var id = Number( new RegExp(/[1-9][0-9]{0,3}/).exec( $(this).text() ) ) || 1;
				var team_no = Number( $(this).parent().attr('class').split(' ')[0].slice(-1) ); // Split the class string, grab the first one (should probably check it) and grab the sub no. from it.
				var mdata = {'monster': id, 'plus_hp': 0, 'plus_atk': 0, 'plus_rcv': 0, 'current_awakening': 0, 'level': 1};
				var monster = new Monster(mdata);
				if (!monster) {
					return;
				}

				addMonsterToTeam(monster, team_no-1, ajxRan[2]);
				ajxRan[2].ind_enh[team_no-1][0] = ajxRan[2][team_no-1].countAwaken(22);
				ajxRan[2].ind_enh[team_no-1][1] = ajxRan[2][team_no-1].countAwaken(23);
				ajxRan[2].ind_enh[team_no-1][2] = ajxRan[2][team_no-1].countAwaken(24);
				ajxRan[2].ind_enh[team_no-1][3] = ajxRan[2][team_no-1].countAwaken(25);
				ajxRan[2].ind_enh[team_no-1][4] = ajxRan[2][team_no-1].countAwaken(26);

				ajxRan[2].ind_orb[team_no-1][0] = ajxRan[2][team_no-1].countAwaken(14);
				ajxRan[2].ind_orb[team_no-1][1] = ajxRan[2][team_no-1].countAwaken(15);
				ajxRan[2].ind_orb[team_no-1][2] = ajxRan[2][team_no-1].countAwaken(16);
				ajxRan[2].ind_orb[team_no-1][3] = ajxRan[2][team_no-1].countAwaken(17);
				ajxRan[2].ind_orb[team_no-1][4] = ajxRan[2][team_no-1].countAwaken(18);
				ajxRan[2].ind_orb[team_no-1][5] = ajxRan[2][team_no-1].countAwaken(29);
				buildTeamTable(ajxRan[2]);
			}
        });

	$('.collapse').collapse();
	
	// This problem, and this solution, has caused a dicks amount of problems.
	$( document ).ajaxComplete( function(a, b, c){
		if (!c.crossDomain){
			return;
		}
		if (ajxRan[0] > 0 && ajxRan[0] == ajxRan[1]){
			buildTeamTable(ajxRan[2]);
		}
		else {
			ajxRan[1] += 1;
		}
	} );

        if (QueryString.team_id) {
            $("#team-id-form input[name=team-id]").val(QueryString.team_id);

            loadTeam();
        }
} );


function recalcMonsters() {
    for (var ii = 0; ii < 6; ii++) {
        var monster = ajxRan[2][ii];

        var current_latents = (monster) ? monster.latents : [];

        if (monster) {
            switch (current_monster_modification) {
            case MONSTER_MOD_NONE:
                monster.id = monster.original_id;
                monster.loadMonsterStats();

                if (monster.current_xp)
                    monster.level = monster.levelFromXP(monster.current_xp);
                else
                    monster.level = monster.current_level;

                monster.pluses = {'hp': monster.plus_hp, 'atk': monster.plus_atk, 'rcv': monster.plus_rcv}

                monster.hp = monster.statCalc('hp');
                monster.atk = monster.statCalc('atk');
                monster.rcv = monster.statCalc('rcv');

                monster.awakenings = monster.all_awake.slice(0, monster.current_awakening);
                monster.latents = current_latents;
                break;
            case MONSTER_MOD_MAX_LEVEL:
                monster.id = monster.max_evo;
                monster.loadMonsterStats();

                monster.level = monster.max_level;

                monster.pluses = {'hp': monster.plus_hp, 'atk': monster.plus_atk, 'rcv': monster.plus_rcv}

                monster.hp = monster.statCalc('hp');
                monster.atk = monster.statCalc('atk');
                monster.rcv = monster.statCalc('rcv');

                monster.awakenings = monster.all_awake;
                monster.latents = current_latents;
                break;
            case MONSTER_MOD_HYPERMAX:
                monster.id = monster.max_evo;
                monster.loadMonsterStats();

                monster.level = monster.max_level;

                monster.pluses = {'hp': 99, 'atk': 99, 'rcv': 99}
                monster.hp = monster.statCalc('hp', true);
                monster.atk = monster.statCalc('atk', true);
                monster.rcv = monster.statCalc('rcv', true);

                monster.awakenings = monster.all_awake;
                monster.latents = current_latents;
            }

            team = ajxRan[2];
            calculateTeamStats(monster, team, ii);
        }
    }
    buildTeamTable(ajxRan[2]);
}

function calculateTeamStats(monster, team, index) {
        team.ind_enh[index][0] = monster.countAwaken(22);
        team.ind_enh[index][1] = monster.countAwaken(23);
        team.ind_enh[index][2] = monster.countAwaken(24);
        team.ind_enh[index][3] = monster.countAwaken(25);
        team.ind_enh[index][4] = monster.countAwaken(26);

        team.ind_orb[index][0] = monster.countAwaken(14);
        team.ind_orb[index][1] = monster.countAwaken(15);
        team.ind_orb[index][2] = monster.countAwaken(16);
        team.ind_orb[index][3] = monster.countAwaken(17);
        team.ind_orb[index][4] = monster.countAwaken(18);
        team.ind_orb[index][5] = monster.countAwaken(29);

        team.sb[index] = monster.countAwaken(21);
        team.sbr[index] = monster.countAwaken(28);
}

function displayAwakenings(monsterPosition) {
    team = ajxRan[2];

    // regular awakenings
    $('#awk-row-2 td.team-pos' + (monsterPosition + 1)).text('');
    for (var j = 0; j < team[monsterPosition].awakenings.length; j++){
        if (awoken_disabled)
            $('#awk-row-2 td.team-pos' + (monsterPosition+1)).append( ("<img src=images/" + team[monsterPosition].awakenings[j] + "_disabled.png />") );
        else
            $('#awk-row-2 td.team-pos' + (monsterPosition+1)).append( ("<img src=images/" + team[monsterPosition].awakenings[j] + ".png />") );
    }

    // latent awakenings
    $('#latent-row-2 td.team-pos' + (monsterPosition + 1)).text('');
    for (var ii = 0; ii < team[monsterPosition].latents.length; ii++) {
        var image_number;
        var latent_name = team[monsterPosition].latents[ii];
        if (latent_name.indexOf("res-") == 0)
            image_number = Number( latent_name.charAt(latent_name.length-1)) + 4;
        else
            image_number = {"hp" : 1, "atk": 2, "rcv": 3}[latent_name];

        $('#latent-row-2 td.team-pos' + (monsterPosition+1)).append("<img src=images/" + image_number + ".png />");
    }
}


function buildTeam(teamData){
    var team = ajxRan[2];

	// Lookup each of the five monsters.
	lookupMonster(teamData.leader, 0, team);

	// Lookup the friend.
	if (teamData.friend_leader){
		teamData.friend_leader = {'monster': teamData.friend_leader};
		teamData.friend_leader.level = teamData.friend_level;
		teamData.friend_leader.current_awakening = teamData.friend_awakening;
		teamData.friend_leader.plus_hp = teamData.friend_hp;
		teamData.friend_leader.plus_atk = teamData.friend_atk;
		teamData.friend_leader.plus_rcv = teamData.friend_rcv;
		var monster = new Monster(teamData.friend_leader);
		ajxRan[1] += 1;
		team[5] = monster;

		team.ind_enh[5][0] = monster.countAwaken(22);
		team.ind_enh[5][1] = monster.countAwaken(23);
		team.ind_enh[5][2] = monster.countAwaken(24);
		team.ind_enh[5][3] = monster.countAwaken(25);
		team.ind_enh[5][4] = monster.countAwaken(26);
		
		team.ind_orb[5][0] = monster.countAwaken(14);
		team.ind_orb[5][1] = monster.countAwaken(15);
		team.ind_orb[5][2] = monster.countAwaken(16);
		team.ind_orb[5][3] = monster.countAwaken(17);
		team.ind_orb[5][4] = monster.countAwaken(18);
		team.ind_orb[5][5] = monster.countAwaken(29);
		
		team.sb[5] = monster.countAwaken(21);
		team.sbr[5] = monster.countAwaken(28);
	}
	else {
		ajxRan[0] -= 1;
		team[5] = undefined;
	}

	for (var i = 1; i <= 4; i++){
		if (teamData['sub' + i]){
			lookupMonster(teamData['sub' + i], i, team);
		}
		// Eh, let's keep the size of the list constant.
		else {
			ajxRan[0] -= 1;
			team[i] == undefined;
		}
	};
}

// TODO: Split into smaller function that works per monster, call in loop/when monster updates.
function buildTeamTable(team, lskill, fskill){
	$('#team-block').addClass('hidden');
	var leadSkill = (typeof lskill == 'undefined' || !lskill) ? [] : lskill;
	var friendSkill = (typeof fskill == 'undefined' || !fskill) ? [] : fskill;

        // use placeholder leader skills if we're entering a team from scratch
        if (! team[0])
            leadSkill = [1, 1, 1];
        if (! team[5])
            friendSkill = [1, 1, 1];

	// Calc the Leader Skill bonus.
	for (var i = 0; i < lead_skills.length; i++){
		if (leadSkill.length && friendSkill.length) {
			break;
		}

		var curr = lead_skills[i];
		if (!leadSkill.length && curr.name == team[0].lead){
			leadSkill = curr.hasOwnProperty('data') ? curr.data : [1, 1, 1];
		}
		if (!friendSkill.length && curr.name == team[5].lead){
			friendSkill = curr.hasOwnProperty('data') ? curr.data : [1, 1, 1];
		}
	}

	function applyLeads(lead, monster){
		// If there's no condition, apply it and move on.
		if (lead.length == 3){
			monster.bonus = [ monster.bonus[0] * lead[0], monster.bonus[1] * lead[1], monster.bonus[2] * lead[2] ];
			return;
		}

		for (var j = 3; j < lead.length; j++){
			// If either condition is met, apply the multiplier and bugger off.
			if (lead[j][0] == "elem"){
				for (var k = 1; (k < lead[j].length && monster.elements.indexOf(lead[j][k]) != -1); k++){
					monster.bonus = [ monster.bonus[0] * lead[0], monster.bonus[1] * lead[1], monster.bonus[2] * lead[2] ];
					return;
				}
			}

			if (lead[j][0] == "type"){
				for (var k = 1; (k < lead[j].length && monster.type.indexOf(lead[j][k]) != -1); k++){
					monster.bonus = [ monster.bonus[0] * lead[0], monster.bonus[1] * lead[1], monster.bonus[2] * lead[2] ];
					return;
				}
			}
		}
	}

	function helper(stat, i){
		var key = ({'hp': 0, 'atk': 1, 'rcv': 2})[stat];
		if (team[i].bonus[key] != 1){
			var rnd = Math.floor(team[i].boosted_stats[key]);
			rnd = rnd % 1 ? rnd.toFixed(0) : rnd;
			$('#' + stat + '-row .team-pos' + (i+1) + ' span').text( rnd );
			$('#' + stat + '-row .team-pos' + (i+1) + ' span').tooltip('hide').attr('title', Math.round(team[i][stat])).tooltip('fixTitle');
		}
		else {
			var rnd = Math.round(team[i][stat]);
			rnd = rnd % 1 ? rnd.toFixed(0) : rnd;
			$('#' + stat + '-row .team-pos' + (i+1) + ' span').text( rnd );
		}
		$('#' + stat + '-row .team-pos' + (i+1) + ' span + span').text(' (+' + team[i].pluses[stat] + ')');
                return rnd;
	}

        var team_hp = 0;
        var team_rcv = 0;
        var dark_resists = 0;
	for (var i = 0; i <= 6; i++){
		if (!team[i]){
			// Use ? image.
			$('.team-img-' + (i+1)).attr('src', 'https://www.padherder.com/static/img/monsters/60x60/0.png');
			continue;
		}

		// TODO: Restructure code to hold Leader + Friend multipliers, then calculate later instead of...this.
		team[i].bonus = [1, 1, 1]; // Reset the bonus before recalculating to avoid continuous scaling.
		applyLeads(leadSkill, team[i]);
		applyLeads(friendSkill, team[i]);
		team[i].boosted_stats = [team[i].hp * team[i].bonus[0], team[i].atk * team[i].bonus[1], team[i].rcv * team[i].bonus[2]];

		$('#id-row th.team-pos' + (i+1) + ' span').text(team[i].id);
		$('.team-img-' + (i+1)).attr('src', team[i].image);
		$('#name-row td.team-pos' + (i+1) + ' span').text(team[i].name);
		$('#level-row td.team-pos' + (i+1) + ' span').text(team[i].level);

                var type_html = "";
                if (team[i].type.length == 1)
                    type_html = types[team[i].type[0]];
                else {
                    for (var ii = 0; ii < team[i].type.length - 1; ii++)
                        type_html += types[team[i].type[ii]] + "<br/>";
                    type_html += types[team[i].type[team[i].type.length - 1]];
                }
		$('#type-row td.team-pos' + (i+1) + ' span').html(type_html);

		$('#lead-row td.team-pos' + (i+1) + ' span').text("x" + (team[i].bonus[0] % 1 ? team[i].bonus[0].toFixed(2) : team[i].bonus[0])
                                                                  + " x" + (team[i].bonus[1] % 1 ? team[i].bonus[1].toFixed(2) : team[i].bonus[1])
                                                                  + " x" + (team[i].bonus[2] % 1 ? team[i].bonus[2].toFixed(2) : team[i].bonus[2]) );

		team_hp += helper('hp', i);
		helper('atk', i);
		team_rcv += helper('rcv', i);
		$('#awk-row td.team-pos' + (i+1) + ' span').text(team[i].awakenings.length);

                // count latent dark resists
                if (team[i].latents)
                    for (var jj = 0; jj < team[i].latents.length; jj++)
                        if (team[i].latents[jj] == "res-4")
                            dark_resists++;
                displayAwakenings(i);
	}
        $('#hp-total').text(Math.round(team_hp));
        $('#rcv-total').text(Math.round(team_rcv));

        // calculate Hera preemptive damage
        var hera_damage = 38910 * (1 - 0.01 * dark_resists);
        $('#hera-damage').text(Math.round(hera_damage));

	for (var i = 0; i < 6; i++){
		team.enhance[i] = team.ind_enh[0][i] + team.ind_enh[1][i] + team.ind_enh[2][i] + team.ind_enh[3][i] + team.ind_enh[4][i] + team.ind_enh[5][i];
		team.orbs[i] = team.ind_orb[0][i] + team.ind_orb[1][i] + team.ind_orb[2][i] + team.ind_orb[3][i] + team.ind_orb[4][i] + team.ind_orb[5][i];
	}

	$('input[name=fr-enhance]').val(team.enhance[0]);
	$('input[name=wt-enhance]').val(team.enhance[1]);
	$('input[name=wd-enhance]').val(team.enhance[2]);
	$('input[name=lt-enhance]').val(team.enhance[3]);
	$('input[name=dk-enhance]').val(team.enhance[4]);
	
	$('input[name=fr-plus]').val(team.orbs[0]);
	$('input[name=wt-plus]').val(team.orbs[1]);
	$('input[name=wd-plus]').val(team.orbs[2]);
	$('input[name=lt-plus]').val(team.orbs[3]);
	$('input[name=dk-plus]').val(team.orbs[4]);

	$('input[name=sb-view]').val(team.sb[0] + team.sb[1] + team.sb[2] + team.sb[3] + team.sb[4] + team.sb[5]);
	$('input[name=sbr-view]').val(team.sbr[0] + team.sbr[1] + team.sbr[2] + team.sbr[3] + team.sbr[4] + team.sbr[5]);

	$('[data-toggle=tooltip]').tooltip();
	$('#team-block').removeClass('hidden');
	running = false;
	return;
}

function lookupMonster(padhID, index, team){
	$.ajax( "https://www.padherder.com/user-api/monster/" + padhID + "/", {dataType: 'json', success: function(a, b, c){
		m = new Monster(a);
                calculateTeamStats(m, team, index);
		addMonsterToTeam( m, index, team );
	}} );
}

function Monster(monsterData){
	/* :: OBJECT METHODS BEGIN BELOW :: */
	this.xpAtLevel = function(level){
		return Math.round(this.xp_curve * Math.pow((level-1)/98, 2.5));
	}

	this.levelFromXP = function(XP){
		return 1 + Math.floor((this.max_level - 1) * Math.pow((XP/Math.max(this.xpAtLevel(this.max_level), 1)), 1/2.5));
	}

	this.countAwaken = function(id){
            if (awoken_disabled) return 0;

		var cnt = 0;
		var awkn = this.awakenings;
		while (awkn.indexOf(id) != -1){
			cnt++;
			awkn = awkn.slice( awkn.indexOf(id)+1 );
		}
		return cnt;
	}

        this.countLatents = function(stat) {
            var count = 0;
            for (var ii = 0; ii < this.latents.length; ii++) {
                if (this.latents[ii] == stat)
                    count++;
            }
            return count;
        }

	this.statCalc = function(stat){
		// MinStat + (MaxStat - MinStat) * (Level-1/MaxLevel-1)^StatGrowth
		var mods = this[stat + '_growth'];
		var key = {'hp': 10, 'atk': 5, 'rcv': 3};
		var awake_key = {'hp': 1, 'atk': 2, 'rcv': 3};
		var awake_stat = {'hp': 200, 'atk': 100, 'rcv': 50};
		var latent_stat = {'hp': 0.015, 'atk': 0.01, 'rcv': 0.04};

		var base_stat = mods['min'] + ((mods['max']-mods['min']) * Math.pow((this.level-1)/Math.max(this.max_level-1, 1), mods['scale']));
                var awaken_stat = this.countAwaken(awake_key[stat]) * awake_stat[stat];
                var plus_stat = this.pluses[stat] * key[stat];
                var latent_multiplier = 1 + (this.countLatents(stat) * latent_stat[stat]);
		var total_stat = latent_multiplier * (base_stat + awaken_stat) + plus_stat;

                return total_stat;
	}

        this.loadMonsterStats = function() {
            var monstFromList = getMonstFromList(this.id);

            if (!monstFromList){
		return;
            }
            this.name = monstFromList.name;
            this.all_awake = monstFromList.awoken_skills;
            this.elements = monstFromList.element2 != null ? [monstFromList.element, monstFromList.element2] : [monstFromList.element];

            var type_array;
            if (monstFromList.type3 != null) type_array = [monstFromList.type, monstFromList.type2, monstFromList.type3];
            else if (monstFromList.type2 != null) type_array = [monstFromList.type, monstFromList.type2];
            else type_array = [monstFromList.type];

            this.type = type_array
            this.lead = monstFromList.leader_skill;
            this.image = "http://www.padherder.com/" + monstFromList.image60_href;
            this.max_level = monstFromList.max_level;
            this.xp_curve = monstFromList.xp_curve;

            this.hp_growth = {'min': monstFromList.hp_min, 'max': monstFromList.hp_max, 'scale': monstFromList.hp_scale};
            this.atk_growth = {'min': monstFromList.atk_min, 'max': monstFromList.atk_max, 'scale': monstFromList.atk_scale};
            this.rcv_growth = {'min': monstFromList.rcv_min, 'max': monstFromList.rcv_max, 'scale': monstFromList.rcv_scale};
            this.latents = [];
        }
	/* :: OBJECT METHODS END ABOVE :: */

	/* :: CONSTRUCTOR BEGINS BELOW :: */
	if (!monsterData){
		return;
	}

        // need this to support showing/reverting max evolution stats
        this.original_id = monsterData.monster;
        this.max_evo = (monsterData.target_evolution) ? (monsterData.target_evolution) : this.original_id

	this.id = monsterData.monster;

        this.loadMonsterStats();

        this.current_awakening = monsterData.current_awakening;
	this.awakenings = this.all_awake.slice(0, monsterData.current_awakening);

        // save each of these separately so that we can revert back to them
        this.plus_hp = monsterData.plus_hp;
        this.plus_atk = monsterData.plus_atk;
        this.plus_rcv = monsterData.plus_rcv;
	this.pluses = {'hp': monsterData.plus_hp, 'atk': monsterData.plus_atk, 'rcv': monsterData.plus_rcv}

	// 1 + MaxLvl * ( (XP/Max)^(1/2.5) )
	// Math.max to stop division by 0, with max level 1 monsters.
	this.level = monsterData.level == undefined ? this.levelFromXP(monsterData.current_xp) : monsterData.level;

        this.current_level = this.level;
        this.current_xp = monsterData.current_xp;

	this.hp = this.statCalc('hp');
	this.atk = this.statCalc('atk');
	this.rcv = this.statCalc('rcv');
	this.bonus = [1, 1, 1];
	this.boosted_stats = [this.hp, this.atk, this.rcv];
	/* :: CONSTRUCTOR ENDS ABOVE :: */
}

function getMonstFromList(id){
	var index = id < monst_list.length ? id : monst_list.length - 1;
	var monster = monst_list[index];
	while (monster.id != id){
		diff = monster.id - index;
		index = diff > 0 ? index - 1 : index + 1;
		monster = monst_list[index];
	}
	return monster;
}

function addMonsterToTeam(monster, index, team){
	team[index] = monster;
	console.log("Monster processed.");
}

function updatePlusFields(clr){
	var contents = $('input[name=' + clr + '-match]').val();

	var combos = contents.split('+');
	var str = "";
	for (var i = 0; i < combos.length; i++){
		//str += i > 0 ? "+" + eval( combos[i].replace('R', '+6') ) : eval( combos[i].replace('R', '+6') );
		// Derp, default to 0. Assume no plus orbs in the combo.
		str += i > 0 ? "+" + 0 : 0;
	}

	$('input[name=' + clr + '-plus]').val(str);
}

function calcDamage(){
	// elem1 = defending, elem2 = attacking
	function getElemMult(elem1, elem2){
		if ((elem1 == 0 && elem2 == 1) || (elem1 == 1 && elem2 == 2) || (elem1 == 2 && elem2 == 0) || (elem1 == 3 && elem2 == 4) || (elem1 == 4 && elem2 == 3)){
			return 2;
		}
		if ((elem2 == 0 && elem1 == 1) || (elem2 == 1 && elem1 == 2) || (elem2 == 2 && elem1 == 0) || (elem2 == 3 && elem1 == 4) || (elem2 == 4 && elem1 == 3)){
			return 0.5;
		}
		return 1;
	}

        function getResistMult(resist_elem1, resist_elem2, resist_percentage, curr_elem) {
            if (((resist_elem1[0] && resist_elem1[0] == "all") ||
                 (resist_elem1[1] && resist_elem1[1] == curr_elem) ||
                 (resist_elem2[1] && resist_elem2[1] == curr_elem)) &&
                resist_percentage)
                return (100 - resist_percentage) / 100;
            else
                return 1;
        }

        function getAbsorbMult(absorb_elem, absorb_threshold, curr_elem, damage) {
            if ((absorb_elem[1] && absorb_elem[1] == curr_elem) ||
                (absorb_threshold && damage > absorb_threshold))
                return -1;
            else
                return 1;
        }

	function getActiveMult(active_skill, leader_conditional, monster, elem){
		elem = typeof elem !== 'undefined' ? elem : -1;
                var active_skill_mult = 1;
                var leader_conditional_mult = 1;

                // TODO: refactor these two for loops; they're the same code.
		for (var j = 3; j < active_skill.length; j++){
			if (active_skill[j][0] == "elem"){
				for (var k = 1; (k < active_skill[j].length && monster.elements.indexOf(active_skill[j][k]) != -1 && active_skill[j].indexOf(elem) != -1); k++){
					active_skill_mult = active_skill[1];
				}
			}
			if (active_skill[j][0] == "type"){
				for (var k = 1; (k < active_skill[j].length && monster.type.indexOf(active_skill[j][k]) != -1); k++){
					active_skill_mult = active_skill[1];
				}
			}
		}
		for (var j = 3; j < leader_conditional.length; j++){
			if (leader_conditional[j][0] == "elem"){
				for (var k = 1; (k < leader_conditional[j].length && monster.elements.indexOf(leader_conditional[j][k]) != -1 && leader_conditional[j].indexOf(elem) != -1); k++){
					leader_conditional_mult = leader_conditional[1];
				}
			}
			if (leader_conditional[j][0] == "type"){
				for (var k = 1; (k < leader_conditional[j].length && monster.type.indexOf(leader_conditional[j][k]) != -1); k++){
					leader_conditional_mult = leader_conditional[1];
				}
			}
                        if (leader_conditional[j][0] == "all")
                            leader_conditional_mult = leader_conditional[1];
		}
		return active_skill_mult * leader_conditional_mult;
	}

	// TODO: Drop "" empty combos from the list, instead of checking everywhere for them.

	// Damage = (Active * Typing * Board * Row Enhance * Attack) - Defense
	var team = ajxRan[2];
	var t_damage = [[], [], [], [], [], []];
	var raw_damage = [0, 0, 0, 0, 0, 0];
	var elem = $('#enemy-elem').val().slice(-1);
	var health = $('#enemy-health').val();
	var defense = $('#enemy-def').val();
	var combos = $('.combo-row');

	// Loading active skill multiplier and conditions.
        if ($('#active-field').val() == "")
            changeActiveSkill(1);
	var act_skl = [1, $('#active-field').val(), 1];
	var cond1 = $('select[name=act-cond1]').val().split('-');
	var cond2 = $('select[name=act-cond2]').val().split('-');

        if (cond1[0]){
            act_skl.push( [cond1[0], eval(cond1[1])] );
        }
        if (cond2[0]){
            act_skl.push( [cond2[0], eval(cond2[1])] );
        }

	// Loading leader skill conditional multiplier
        if ($('#leader-field').val() == "")
            changeLeaderSkill(1);
	var leader_conditional = [1, $('#leader-field').val(), 1];
	var lead_cond1 = $('select[name=leader-cond1]').val().split('-');
	var lead_cond2 = $('select[name=leader-cond2]').val().split('-');

        if (lead_cond1[0]){
            leader_conditional.push( [lead_cond1[0], eval(lead_cond1[1])] );
        }
        if (lead_cond2[0]){
            leader_conditional.push( [lead_cond2[0], eval(lead_cond2[1])] );
        }

        // Loading enemy resist multiplier
        var resist_elem1 = $('select[id=enemy-resist1]').val().split('-');
        var resist_elem2 = $('select[id=enemy-resist2]').val().split('-');
        var resist_percentage = $('input[id=resist-percentage]').val();

        // Loading enemy absorb multiplier
        var absorb_elem = $('select[id=enemy-absorb]').val().split('-');
        var absorb_threshold = $('input[id=absorb-threshold]').val();

	// BOARD
	var combo_count = combos.length;
	var rows = [0, 0, 0, 0, 0, 0];
	var tpa_section = [0, 0, 0, 0, 0];
	var colour_contrib = [0, 0, 0, 0, 0, 0];
	for (var i = 0; i < combos.length; i++){
		var node = $('.combo-row:nth-of-type(' + (i+1) + ')');
		var combo_size = Number(node.find('input[name=orbs]').val());
		if (!combo_size){
			combo_count--;
			continue;
		}

		var curr_elem = $('#combo-drop-' + (i+1)).val();
		var p_o = 0;

		// Get the plus_orb info.
                p_o = node.find('input[name=porb]').val();
                // If there's more +orbs than combo size, then something messed up. Use combo.
                p_o = p_o > combo_size ? combo_size : p_o;

                // orb enhance bonus and combo size bonus
                var orb_enhance_bonus = (awoken_disabled) ? 1 : (1 + (team.orbs[curr_elem] * 0.05) * (p_o > 0));
		var dam = (1 + (p_o * 0.06)) * (1 + ((combo_size-3)/4)) * orb_enhance_bonus;

		// TPA procs
		rows[curr_elem] += node.find('input[type=checkbox]').prop('checked')? 1 : 0;
		if (combo_size == 4 && curr_elem != 5 && !awoken_disabled){
			tpa_section[curr_elem] += dam;
		}
		else {
			colour_contrib[curr_elem] += dam;
		}
	}

        // apply bonuses for rows and total combos
	var combo_contrib = 1 + ((combo_count - 1)/4);
	for (var i = 0; i <= 5; i++){

            // bonus from rows - applies to all combos of that color
            var row_contrib = (awoken_disabled) ? 1 : 1 + (0.1 * rows[i] * team.enhance[i]);
            var dam_bonus = row_contrib * combo_contrib;

            raw_damage[i] = [dam_bonus * colour_contrib[i], dam_bonus * tpa_section[i]];
	}

	var curr_health = health;
        var t_dam = 0;

        for (var j = 0; j < 2; j++) {

            for (var i = 0; i < 6; i++){
		if (!team[i] || typeof team[i].elements[j] == 'undefined' || sub_binds[i]) {
			$('#results-block .team-img-' + (i+1) + ' ~ .dmg-' + (j+1)).text(0);
			continue;
		}

			var curr_elem = team[i].elements[j];
			var elem_bonus = getElemMult(elem, curr_elem);
			var resist_multiplier = getResistMult(resist_elem1, resist_elem2, resist_percentage, curr_elem);
			var attack = team[i].boosted_stats[1];
			var mult_bonus = getActiveMult(act_skl, leader_conditional, team[i], curr_elem);

			// If there's no damage for this element, put a 0 and move on.
                        if ((raw_damage[curr_elem][0] + raw_damage[curr_elem][1]) == 0){
				$('#results-block .team-img-' + (i+1) + ' ~ .dmg-' + (j+1)).text(0);
				continue;
                        }

			// Sub-element check.
			attack = j == 1 ? (team[i].elements[0] == team[i].elements[1] ? attack * 0.1 : attack * 0.3) : attack;
			var tpa_bonus = Math.pow(1.5, team[i].countAwaken(27));
			var dam = (mult_bonus *
                                   (raw_damage[curr_elem][0] + (raw_damage[curr_elem][1] * tpa_bonus)) *
                                   elem_bonus *
                                   resist_multiplier *
                                   attack
                                   ) - defense;
			dam = Math.max(1, Math.round(dam));

                        // check if damage was absorbed
                        var absorb_multiplier = getAbsorbMult(absorb_elem, absorb_threshold, curr_elem, dam);
                        dam *= absorb_multiplier;

			$('#results-block .team-img-' + (i+1) + ' ~ .dmg-' + (j+1)).text( dam.toLocaleString() );

                        // figure out how much actual damage we did, given that we cannot go below 0 or above max health
                        var actual_damage = Math.max(curr_health - health, Math.min(curr_health, dam));
                        curr_health -= actual_damage;
                        t_dam += dam;

                        // DEBUG
                        //console.log("Damage done by monster " + i + ": " + actual_damage + "; Current health: " + curr_health);
		}
	}

        // if we spiked the monster, show overkill
        if (curr_health != 0)
            t_dam = health - curr_health;

        // now calculate RCV
        var health_recovered = Math.round($('#rcv-total').text() * raw_damage[5][0]);
        $('#results-block #health-result').text( health_recovered.toLocaleString() );

        // display the results
	$('#results-block #tdam-result').text( t_dam.toLocaleString() );
	$('#results-block #calc-result').text( curr_health == 0 ? 'Spiked! 0 HP!' : curr_health.toLocaleString() + " HP" );
	$('#results-block').removeClass('hidden');

        if (health == "") {
            $('#results-block #calc-after').addClass('hidden');
            $('#results-block #calc-result').addClass('hidden');
        }
        else {
            $('#results-block #calc-after').removeClass('hidden');
            $('#results-block #calc-result').removeClass('hidden');
        }
}
